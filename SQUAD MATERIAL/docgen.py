import os
import json
import tqdm
import pickle
import en_core_web_trf
import pandas as pd

tokenizer = en_core_web_trf.load()

def normalize_path(path):
    for rep in (' ', '-', ':', '.'):
        path = path.replace(rep, '_')
    return path

def save_data(path, data):
    with open(path, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

def load_data(path):
    with open(path, 'rb') as handle:
        result = pickle.load(handle)
    return result

def normalize(text):
    return text.strip()

def preprocess(text):
    return [
        {
            'text': term.text,
            'i': term.i,
            'head': term.head.text,
            'head_i': term.head.i,
            'left_edge': term.left_edge.text,
            'left_edge_i': term.left_edge.i,
            'right_edge': term.right_edge.text,
            'right_edge_i': term.right_edge.i,
            'ent_type': term.ent_type,
            'ent_type_': term.ent_type_,
            'ent_iob': term.ent_iob,
            'lemma': term.lemma,
            'lemma_': term.lemma_,
            'is_alpha': term.is_alpha,
            'is_ascii': term.is_ascii,
            'is_digit': term.is_digit,
            'is_title': term.is_title,
            'is_punct': term.is_punct,
            'is_space': term.is_space,
            'like_url': term.like_url,
            'like_num': term.like_num,
            'like_email': term.like_email,
            'is_oov': term.is_oov,
            'is_stop': term.is_stop,
            'pos': term.pos,
            'pos_': term.pos_,
            'tag': term.tag,
            'tag_': term.tag_,
            'dep': term.dep,
            'dep_': term.dep_,
            'prob': term.prob,
            'idx': term.idx,
            'sentiment': term.sentiment,
            'lex_id': term.lex_id,
            'rank': term.rank,
            'cluster': term.cluster
        }
        for term in tokenizer(normalize(text))
    ]

base_path = 'training_set.json'
partial_path = 'training_set.pd.json'
if not os.path.exists(partial_path):
    with open(base_path) as dataset_file:
        dataset_json = json.load(dataset_file)

    df = []
    par_id = 0
    for doc_id, doc in enumerate(dataset_json['data']):
        for par_index, paragraph in enumerate(doc['paragraphs']):
            for question in paragraph['qas']:
                df.append([
                    question['id'],
                    doc_id,
                    doc['title'],
                    normalize(question['question']),
                    question['answers'][0]['answer_start'],
                    normalize(question['answers'][0]['text']),
                    par_id,
                    par_index,
                    normalize(paragraph['context']),
                ])
            par_id += 1
    df = pd.DataFrame(df, columns=['id', 'doc', 'title', 'question', 'answer_start', 'answer', 'par_id', 'par_index', 'paragraph'])
    df['id'] = df['id'].astype('string')
    df['doc'] = pd.to_numeric(df['doc'], downcast='unsigned')
    df['title'] = df['title'].astype('category')
    df['question'] = df['question'].astype('string')
    df['answer_start'] = pd.to_numeric(df['answer_start'], downcast='unsigned')
    df['answer'] = df['answer'].astype('string')
    df['par_id'] = pd.to_numeric(df['par_id'], downcast='unsigned')
    df['par_index'] = pd.to_numeric(df['par_index'], downcast='unsigned')
    df['paragraph'] = df['paragraph'].astype('string')
    df.info()

    df.to_json(partial_path)

df = pd.read_json(partial_path)
docs = {}
qsts = {}

os.makedirs('docs', exist_ok=True)
dfit = tqdm.tqdm(df.iloc, total=len(df))

for id, doc_id, title, question, answer_start, answer, par_id, par_idx, paragraph in dfit:
    if par_idx not in docs:
        doc_file = f'docs/doc_{par_id}.pkl'
        if os.path.exists(doc_file):
            dfit.set_description(f'[CACHE] - {title} - {par_id}')
            docs[par_idx] = load_data(doc_file)
        else:
            dfit.set_description(f'[DATA] - {title} - {par_id}')
            doc = preprocess(paragraph)
            doc = [
                dict(paragraph_id=par_id, document_id=doc_id, title=title, token_index=tok_id, **tokens)
                for tok_id, tokens in enumerate(doc)
            ]
            docs[par_idx] = doc
            save_data(doc_file, doc)

    qst_file = f'docs/qst_{id}.pkl'
    qid = int(id, 16)
    if os.path.exists(qst_file):
        dfit.set_description(f'[CACHE] - {id} - {qid}')
        qsts[qid] = load_data(qst_file)
    else:
        dfit.set_description(f'[DATA] - {id} - {qid}')
        qst = preprocess(question)
        qst = [
            dict(id=id, document_id=doc_id, title=title, paragraph_id=par_id, token_index=tok_id, **tokens)
            for tok_id, tokens in enumerate(qst)
        ]
        qsts[qid] = qst
        save_data(qst_file, qst)

df_doc = pd.DataFrame([ row for doc in docs.values() for row in doc ], columns=[
    'paragraph_id', 'document_id', 'title', 'token_index', 
    'text', 'i', 'head', 'head_i', 'left_edge',
    'left_edge_i', 'right_edge', 'right_edge_i',
    'ent_type', 'ent_type_', 'ent_iob', 'lemma',
    'lemma_', 'is_alpha', 'is_ascii', 'is_digit',
    'is_title', 'is_punct', 'is_space', 'like_url',
    'like_num', 'like_email', 'is_oov', 'is_stop',
    'pos', 'pos_', 'tag', 'tag_', 'dep', 'dep_',
    'prob', 'idx', 'sentiment', 'lex_id', 'rank',
    'cluster'
])
for k in [ 'paragraph_id', 'document_id', 'token_index', 'i', 'head_i', 'left_edge_i', 'right_edge_i', 'ent_type', 'ent_iob', 'lemma', 'pos', 'tag', 'dep', 'prob', 'idx', 'sentiment', 'lex_id', 'rank', 'cluster' ]:
    df_doc[k] = pd.to_numeric(df_doc[k], downcast='unsigned')
for k in ['title', 'text', 'head', 'left_edge', 'right_edge', 'lemma_', ]:
    df_doc[k] = df_doc[k].astype('string')
for k in ['ent_type_', 'pos_', 'tag_', 'ent_type_', 'dep_']:
    df_doc[k] = df_doc[k].astype('category')
df_doc.to_pickle('docgen_doc.pkl')
del df_doc

df_qst = pd.DataFrame([ row for qst in qsts.values() for row in qst ], columns=[
    'id', 'document_id', 'title', 'paragraph_id', 'token_index',
    'text', 'i', 'head', 'head_i', 'left_edge',
    'left_edge_i', 'right_edge', 'right_edge_i',
    'ent_type', 'ent_type_', 'ent_iob', 'lemma',
    'lemma_', 'is_alpha', 'is_ascii', 'is_digit',
    'is_title', 'is_punct', 'is_space', 'like_url',
    'like_num', 'like_email', 'is_oov', 'is_stop',
    'pos', 'pos_', 'tag', 'tag_', 'dep', 'dep_',
    'prob', 'idx', 'sentiment', 'lex_id', 'rank',
    'cluster'
])
for k in ['id', 'title', 'text', 'head', 'left_edge', 'right_edge', 'lemma_', ]:
    df_qst[k] = df_qst[k].astype('string')
for k in [ 'paragraph_id', 'document_id', 'token_index', 'i', 'head_i', 'left_edge_i', 'right_edge_i', 'ent_type', 'ent_iob', 'lemma', 'pos', 'tag', 'dep', 'prob', 'idx', 'sentiment', 'lex_id', 'rank', 'cluster' ]:
    df_qst[k] = pd.to_numeric(df_qst[k], downcast='unsigned')
for k in ['ent_type_', 'pos_', 'tag_', 'ent_type_', 'dep_']:
    df_qst[k] = df_qst[k].astype('category')
df_qst.to_pickle('docgen_qst.pkl')
del df_qst
